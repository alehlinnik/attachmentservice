module.exports = function (app) {

    function modifyViewFiles(ctx, model, next) {
        if (ctx.result.viewurl != 'uploading' && ctx.result.viewurl != 'not found') {
            ctx.res.status(200);
            ctx.res.send(ctx.result)
        } else
            if (ctx.result.viewurl == 'uploading')
                ctx.res.status(202).end();
            else
                ctx.res.status(404).end();
    }
    function modifyStatusFiles(ctx, model, next) {
        if (ctx.result.fileslink != 'uploading' && ctx.result.fileslink != 'not found') {
            ctx.res.status(201);
            ctx.res.send(ctx.result)
        } else
            if (ctx.result.fileslink == 'uploading')
                ctx.res.status(102).end();
            else
                ctx.res.status(404).end();
    }
    function modifyStatusViewerFiles(ctx, model, next) {
        if (ctx.result.fileslink != 'uploading' && ctx.result.fileslink != 'not found')
            ctx.res.status(201);
        else
            if (ctx.result.fileslink == 'uploading')
                ctx.res.status(102).end();
            else
                ctx.res.status(404).end();
    }
    app.models.Attachment.afterRemote('viewFiles', modifyViewFiles);
    app.models.Attachment.afterRemote('statusFiles', modifyStatusFiles);
    app.models.Attachment.afterRemote('statusViewerFiles', modifyStatusViewerFiles);
};