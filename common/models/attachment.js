process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

const modelDisable = require('../helper/modelhelper.js');
var request = require('request');
var parseString = require('xml2js').parseString;
var str2json = require('string-to-json');
var config = require('./config.json');

var uploading = {};
var uploadingViewer = {};
//requests options
var getAttachmentRequestOptions = {
    encoding: null,
    method: 'GET',
    auth: {
        user: config.domino.user,
        pass: config.domino.password,
        sendImmediately: true
    }
}
var postAttachmentRequestOptions = {
    method: 'POST',
    auth: {
        user: config.connections.user,
        pass: config.connections.password,
        sendImmediately: true
    }
}
var postViewerRequestOptions = {
    method: 'POST',
    auth: {
        user: config.domino.user,
        pass: config.domino.password,
        sendImmediately: true
    }
}
//api otions
var uploadFileOptions = {
    accepts: [{
        arg: 'docId',
        type: 'string',
        description: 'The unid of the mail document.',
        http: { source: 'header' },
        required: true,
        default: 'FD19E8B85D6D9A1C86257FFC0064605F'
    }, {
            arg: 'attachName',
            type: 'string',
            description: 'The actual saved name of the attachment. If two attachments with same name are' +
            ' saved in an email, SCN generates different saved name for these attachments, we need to pass' +
            ' the actual saved name instead of the visiable name.',
            http: { source: 'header' },
            required: true,
            default: 'test.txt'
        }, {
            arg: 'updateExistingFile',
            type: 'boolean',
            description: 'The value can be true of false, default is false. This tells the Attachment' +
            ' Transfer Service whether we want to update an existing Connections files with new version.',
            http: { source: 'header' },
            required: false
        }, {
            arg: 'uploadFileName',
            type: 'string',
            description: 'This is used to specify the file name in Connections Files. In case there is' +
            ' already an existing file with same name in Connections, and end user wants to upload the' +
            ' attachment with a different name, this property is used to provide the new name.',
            http: { source: 'header' },
            required: false
        }, {
            arg: 'summary',
            type: 'string',
            description: 'The summary to the uploaded file in Connections.',
            http: { source: 'header' },
            required: false
        }],
    returns: {
        arg: 'statusurl',
        type: 'string'
    },
    http: { path: '/upload/files', verb: 'post', status: 202 },
    description: "View mail attachment within the Docs server's viewer service"
}
var viewOptions = {
    accepts: [{
        arg: 'docId',
        type: 'string',
        description: 'The unid of the mail document.',
        http: { source: 'path' },
        required: true
    }, {
            arg: 'fileName',
            type: 'string',
            description: 'The actual saved name of the attachment. If two attachments with same name are' +
            ' saved in an email, SCN generates different saved name for these attachments, we need to pass' +
            ' the actual saved name instead of the visiable name.',
            http: { source: 'path' },
            required: true
        }],
    returns: [{
        arg: 'viewurl',
        type: 'string'
    }, {
            arg: 'statusurl',
            type: 'string'
        }],
    http: { path: '/view/:docId/File/:fileName', verb: 'get' },
    description: 'Transfer mail attachment file from SCN to Connections.'
}
var statusOptions = {
    accepts: [{
        arg: 'docId',
        type: 'string',
        description: 'The unid of the mail document.',
        http: { source: 'path' },
        required: true
    }, {
            arg: 'fileName',
            type: 'string',
            description: 'The actual saved name of the attachment. If two attachments with same name are' +
            ' saved in an email, SCN generates different saved name for these attachments, we need to pass' +
            ' the actual saved name instead of the visiable name.',
            http: { source: 'path', status: 102 },
            required: true
        }],
    returns: {
        arg: 'fileslink',
        type: 'string'
    },
    http: { path: '/upload/files/status/:docId/File/:fileName', verb: 'get' },
    description: 'This API is used to poll the file upload status.'
}
var statusViewerOptions = {
    accepts: [{
        arg: 'docId',
        type: 'string',
        description: 'The unid of the mail document.',
        http: { source: 'path' },
        required: true
    }, {
            arg: 'fileName',
            type: 'string',
            description: 'The actual saved name of the attachment. If two attachments with same name are' +
            ' saved in an email, SCN generates different saved name for these attachments, we need to pass' +
            ' the actual saved name instead of the visiable name.',
            http: { source: 'path', status: 102 },
            required: true
        }],
    returns: {
        arg: 'fileslink',
        type: 'string'
    },
    http: { path: '/view/status/:docId/File/:fileName', verb: 'get' },
    description: 'This API is used to poll the file viewer upload status.'
}
//function for upload file
function postAttachment(attach, name, docId, summary) {
    postAttachmentRequestOptions.headers = { 'Slug': name }
    postAttachmentRequestOptions.url = config.connections.url + config.connections.uploadUrl;
    if (summary != null) postAttachmentRequestOptions.url += '?shareSummary=' + summary;
    postAttachmentRequestOptions.body = attach;
    request(postAttachmentRequestOptions, function returnPostStatus(err, httpResponse, body) {
        if (err) console.log(err);
        else parseString(body, function responseXML2Json(err, result) {
            if (err) console.log(err);
            else uploading[docId][name] = result.entry['td:uuid'];
        });
    });
}

module.exports = function (Attachment) {

    Attachment.uploadFiles = function (docId, attachOrigName, updateExistingFile, uploadFileName, summary, cb) {
        //getting up requests
        //var attchName='';
        getAttachmentRequestOptions.url = config.domino.url + config.domino.mailDB_Path + '/0/' + docId + '/$File/' + attachOrigName;
        if (uploadFileName != null) attachName = uploadFileName;
        else attachName = attachOrigName;
        postViewerRequestOptions.url = config.domino.url + config.domino.mailDB_Path + '/0/' + docId + config.domino.viewerURLParams + attachOrigName;
        //download file
        request(getAttachmentRequestOptions, function returnAtachment(err, httpResponse, body) {
            if (err) console.log(err);
            //upload file
            else postAttachment(body, attachName, docId, summary);
        });
        request(postViewerRequestOptions, function returnHash(err, httpResponse, body) {
            if (err) console.log(err);
            //upload file
            else {
                var hash = JSON.parse(body);
                //  console.log(hash);
                //  console.log(hash.items);
                if (typeof (uploadingViewer[docId]) != "undefined") {
                    uploadingViewer[docId][attachName] = [];
                    uploadingViewer[docId][attachName].push(hash.items);
                } else {
                    uploadingViewer[docId] = [];
                    uploadingViewer[docId][attachName] = [];
                    uploadingViewer[docId][attachName].push(hash.items);
                }
            }
        });

        //set uploading status
        if (typeof (uploading[docId]) != "undefined") {
            uploading[docId][attachName] = [];
            uploading[docId][attachName].push('uploading');
        } else {
            uploading[docId] = [];
            uploading[docId][attachName] = [];
            uploading[docId][attachName].push('uploading');
        }
        //return url for status
        cb(null, config.serviceUrl + '/attachservice/upload/files/status/' + docId + '/File/' + attachName);
    };

    Attachment.viewFiles = function (docId, fileName, cb) {
        //get status
        var url = 1;
        if (typeof (uploading[docId]) != "undefined")
            if (typeof (uploading[docId][fileName]) != "undefined")
                if (uploading[docId][fileName] != "uploading") {
                    url = 2;
                    viewOptions.http.status = 202;
                } else url = 0;
        //return viewer url
        if (url == 2) cb(null, config.connections.url + 'viewer/app/mail/' + uploadingViewer[docId][fileName], config.serviceUrl + '/attachservice/view/status/' + docId + '/File/' + fileName);
        else if (url == 0) cb(null, 'uploading', config.domino.url + config.domino.mailDB_Path + '/0/' + docId + '/File/' + fileName);
        else cb(null, 'not found', null);
    };

    Attachment.statusViewerFiles = function (docId, fileName, cb) {
        //get status
        var url = 1;
        if (typeof (uploading[docId]) != "undefined")
            if (typeof (uploading[docId][fileName]) != "undefined")
                if (uploading[docId][fileName] != "uploading") {
                    url = 2;
                    statusOptions.http.status = 201;
                } else url = 0;
        //return status
        if (url == 2) cb(null, config.connections.url + 'view/status/' + uploadingViewer[docId][fileName]);
        else if (url == 0) cb(null, 'uploading');
        else cb(null, 'not found');
    }

    Attachment.statusFiles = function (docId, fileName, cb) {
        //get status
        var url = 1;
        if (typeof (uploading[docId]) != "undefined")
            if (typeof (uploading[docId][fileName]) != "undefined")
                if (uploading[docId][fileName] != "uploading") {
                    url = 2;
                    statusOptions.http.status = 201;
                } else url = 0;
        //return status
        if (url == 2) cb(null, config.connections.url + 'files/app#/file/' + uploading[docId][fileName]);
        else if (url == 0) cb(null, 'uploading');
        else cb(null, 'not found');
    }
    //api
    Attachment.remoteMethod('statusViewerFiles', statusViewerOptions);
    Attachment.remoteMethod('statusFiles', statusOptions);
    Attachment.remoteMethod('viewFiles', viewOptions);
    Attachment.remoteMethod('uploadFiles', uploadFileOptions);

};
