/*
 * Copyright (c) 2016 TopCoder, Inc. All Rights Reserved.
 */
/**
 * Contains validation functions
 *
 * @author TCSCODER
 * @version 1.0
 */

/* eslint-disable strict */
'use strict';
/* eslint-enable strict */

const validator = require('rox').validator;

/**
 * Define a global function used for validation.
 * @param {Object} input the object to validate
 * @param {Object} definition the definition object. Refer to rox module for more details.
 * @param {String} [prefix] the prefix for error message.
 * @returns {Error|Null} throws error if validation failed or returns null if validation passed.
 */
function validate(input, definition, prefix) {
  const error = validator.validate(prefix || 'prefix-to-remove', input, definition);
  if (!error) {
    return null;
  }
  // remove prefix in error message
  error.message = error.message.replace('prefix-to-remove.', '');
  // if input is invalid then change the name to input
  error.message = error.message.replace('prefix-to-remove', 'input');
  error.statusCode = 400;
  throw error;
}

// Registering Date type
validator.registerType({
  name: 'Date',
  /**
   * Validate if value is a Date or a string in ISO 8601 date-time format
   * @param {String} name the property name
   * @param {*} value the value to check
   * @returns {Error|Null} null if type is correct or error if incorrect
   */
  validate(name, value) {
    const date = typeof value === 'string' ?
      new Date(Date.parse(value)) : value;
    if (date instanceof Date && date.toString() !== 'Invalid Date') {
      return null;
    }
    return new Error(`${name} must be a valid date or a string in ISO 8601 format`);
  },
});

// Registering some useful type aliases
validator.registerAlias('String?', { type: String, required: false });
validator.registerAlias('StringArray?', { type: [String], required: false, empty: true });
validator.registerAlias('Integer?', { type: 'Integer', required: false });
validator.registerAlias('Boolean?', { type: 'Boolean', required: false });
validator.registerAlias('Date?', { type: 'Date', required: false });

module.exports = {
  validate,
};
