/**
 * Copyright (c) 2016, Topcoder Inc. All rights reserved.
 */
/**
 * Contains helper methods working with models
 *
 * @author TCSCODER
 * @version 1.0
 */

/* eslint-disable strict */
'use strict';
/* eslint-enable strict */

const co = require('co');

/**
 * Helper function for disabling default remote methods for a model
 * @param {Object} targetModel the model
 */
function modelDisable(targetModel) {
  targetModel.sharedClass.methods().forEach((method) => {
    targetModel.disableRemoteMethod(method.name, method.isStatic);
  });
}

/**
 * Helper function for adding 'generator' remote method to a model
 * @param {Object} model the model to add method to
 * @param {Function} method 'generator' function that implements the method
 * @param {Object} options the options object to be passed to Model.remoteMethod()
 */
function addRemoteMethod(model, method, options) {
  /* eslint-disable no-param-reassign */
  model[method.name] = function wrapper() {
  /* eslint-enable no-param-reassign */
    /* eslint-disable prefer-rest-params */
    const cb = arguments[arguments.length - 1];
    co(method.apply(this, arguments))
      .then(result => cb(null, result))
      .catch(err => cb(err));
    /* eslint-enable prefer-rest-params */
  };
  model.remoteMethod(method.name, options);
}

module.exports = modelDisable; // setting modelDisable as the main export for compatibility
                               // with existing code

module.exports.modelDisable = modelDisable;
module.exports.addRemoteMethod = addRemoteMethod;
